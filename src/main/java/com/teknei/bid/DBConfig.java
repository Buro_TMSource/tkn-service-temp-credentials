package com.teknei.bid;

import com.teknei.bid.crypto.Decrypt;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.jdbc.datasource.DriverManagerDataSource;

import javax.sql.DataSource;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

@Configuration
public class DBConfig {

    @Value("${spring.datasource.driver-class-name}")
    private String diver;
    @Value("${spring.datasource.url}")
    private String url;
    @Value("${spring.datasource.username}")
    private String username;
    @Value("${spring.datasource.password}")
    private String password;
    @Value("${tkn.crypto.secret}")
    private String secretName;
    private static final Logger log = LoggerFactory.getLogger(DBConfig.class);
    @Autowired
    private Decrypt decrypt;

    @Bean
    @Primary
    public DataSource dataSource() {
        final String secretUri = "/run/secrets/"+secretName;
        String user = username;
        String pass = password;
        String uri = url;
        try {
            String content = new String(Files.readAllBytes(Paths.get(secretUri)));
            JSONObject jsonObject = new JSONObject(content);
            if (content == null || content.isEmpty()) {
                log.error("No secret supplied, leaving default");
            } else {
                user = jsonObject.optString("username", username);
                pass = jsonObject.optString("password", password);
                uri = jsonObject.optString("url", url);
                user = decrypt(user);
                pass = decrypt(pass);
                uri = decrypt(uri);
            }
        } catch (IOException e) {
            log.error("No secret supplied, leaving default");
        }
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(diver);
        dataSource.setPassword(pass);
        dataSource.setUsername(user);
        dataSource.setUrl(uri);
        return dataSource;
    }

    private String decrypt(String source) {
        try {
            String decrypted = decrypt.decrypt(source);
            return decrypted == null ? source : decrypted;
        } catch (Exception e) {
            log.warn("No ciphered content, returning clear");
            return source;
        }
    }

}