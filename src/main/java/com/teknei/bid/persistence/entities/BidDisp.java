package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "bid_disp", schema = "bid", catalog = "bid")
public class BidDisp {
    private Long idDisp;
    private String descDisp;
    private String numSeri;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;

    @Id
    @Column(name = "id_disp")
    public Long getIdDisp() {
        return idDisp;
    }

    public void setIdDisp(Long idDisp) {
        this.idDisp = idDisp;
    }

    @Basic
    @Column(name = "desc_disp")
    public String getDescDisp() {
        return descDisp;
    }

    public void setDescDisp(String descDisp) {
        this.descDisp = descDisp;
    }

    @Basic
    @Column(name = "num_seri")
    public String getNumSeri() {
        return numSeri;
    }

    public void setNumSeri(String numSeri) {
        this.numSeri = numSeri;
    }

    @Basic
    @Column(name = "id_esta")
    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BidDisp bidDisp = (BidDisp) o;
        return Objects.equals(idDisp, bidDisp.idDisp) &&
                Objects.equals(descDisp, bidDisp.descDisp) &&
                Objects.equals(numSeri, bidDisp.numSeri) &&
                Objects.equals(idEsta, bidDisp.idEsta) &&
                Objects.equals(idTipo, bidDisp.idTipo) &&
                Objects.equals(usrCrea, bidDisp.usrCrea) &&
                Objects.equals(fchCrea, bidDisp.fchCrea) &&
                Objects.equals(usrModi, bidDisp.usrModi) &&
                Objects.equals(fchModi, bidDisp.fchModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idDisp, descDisp, numSeri, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi);
    }
}
