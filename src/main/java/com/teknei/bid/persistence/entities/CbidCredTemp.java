package com.teknei.bid.persistence.entities;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;

@Entity
@Table(name = "cbid_cred_temp", schema = "cbid", catalog = "bid")
public class CbidCredTemp {
    private Long idCredTemp;
    private String usua;
    private String pass;
    private String nomOpe;
    private String apePate;
    private String apeMate;
    private Integer noDact;
    private Long idDisp;
    private Long idEstaCred;
    private Integer idEsta;
    private Integer idTipo;
    private String usrCrea;
    private Timestamp fchCrea;
    private String usrModi;
    private Timestamp fchModi;
    private String usrOpeCrea;
    private String usrOpeModi;

    @Id
    @SequenceGenerator(schema = "cbid", sequenceName = "cbid.cbid_cred_temp_id_cred_temp_seq", allocationSize = 1, name = "cbid_temp_gen", catalog = "bid_db")
    @GeneratedValue(generator = "cbid_temp_gen", strategy = GenerationType.SEQUENCE)
    @Column(name = "id_cred_temp")
    public Long getIdCredTemp() {
        return idCredTemp;
    }

    public void setIdCredTemp(Long idCredTemp) {
        this.idCredTemp = idCredTemp;
    }

    @Basic
    @Column(name = "usua")
    public String getUsua() {
        return usua;
    }

    public void setUsua(String usua) {
        this.usua = usua;
    }

    @Basic
    @Column(name = "pass")
    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    @Basic
    @Column(name = "nom_ope")
    public String getNomOpe() {
        return nomOpe;
    }

    public void setNomOpe(String nomOpe) {
        this.nomOpe = nomOpe;
    }

    @Basic
    @Column(name = "ape_pate")
    public String getApePate() {
        return apePate;
    }

    public void setApePate(String apePate) {
        this.apePate = apePate;
    }

    @Basic
    @Column(name = "ape_mate")
    public String getApeMate() {
        return apeMate;
    }

    public void setApeMate(String apeMate) {
        this.apeMate = apeMate;
    }

    @Basic
    @Column(name = "no_dact")
    public Integer getNoDact() {
        return noDact;
    }

    public void setNoDact(Integer noDact) {
        this.noDact = noDact;
    }

    @Basic
    @Column(name = "id_disp")
    public Long getIdDisp() {
        return idDisp;
    }

    public void setIdDisp(Long idDisp) {
        this.idDisp = idDisp;
    }

    @Basic
    @Column(name = "id_esta_cred")
    public Long getIdEstaCred() {
        return idEstaCred;
    }

    public void setIdEstaCred(Long idEstaCred) {
        this.idEstaCred = idEstaCred;
    }

    @Basic
    @Column(name = "id_esta")
    public Integer getIdEsta() {
        return idEsta;
    }

    public void setIdEsta(Integer idEsta) {
        this.idEsta = idEsta;
    }

    @Basic
    @Column(name = "id_tipo")
    public Integer getIdTipo() {
        return idTipo;
    }

    public void setIdTipo(Integer idTipo) {
        this.idTipo = idTipo;
    }

    @Basic
    @Column(name = "usr_crea")
    public String getUsrCrea() {
        return usrCrea;
    }

    public void setUsrCrea(String usrCrea) {
        this.usrCrea = usrCrea;
    }

    @Basic
    @Column(name = "fch_crea")
    public Timestamp getFchCrea() {
        return fchCrea;
    }

    public void setFchCrea(Timestamp fchCrea) {
        this.fchCrea = fchCrea;
    }

    @Basic
    @Column(name = "usr_modi")
    public String getUsrModi() {
        return usrModi;
    }

    public void setUsrModi(String usrModi) {
        this.usrModi = usrModi;
    }

    @Basic
    @Column(name = "fch_modi")
    public Timestamp getFchModi() {
        return fchModi;
    }

    public void setFchModi(Timestamp fchModi) {
        this.fchModi = fchModi;
    }

    @Basic
    @Column(name = "usr_ope_crea")
    public String getUsrOpeCrea() {
        return usrOpeCrea;
    }

    public void setUsrOpeCrea(String usrOpeCrea) {
        this.usrOpeCrea = usrOpeCrea;
    }

    @Basic
    @Column(name = "usr_ope_modi")
    public String getUsrOpeModi() {
        return usrOpeModi;
    }

    public void setUsrOpeModi(String usrOpeModi) {
        this.usrOpeModi = usrOpeModi;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CbidCredTemp that = (CbidCredTemp) o;
        return Objects.equals(idCredTemp, that.idCredTemp) &&
                Objects.equals(usua, that.usua) &&
                Objects.equals(pass, that.pass) &&
                Objects.equals(nomOpe, that.nomOpe) &&
                Objects.equals(apePate, that.apePate) &&
                Objects.equals(apeMate, that.apeMate) &&
                Objects.equals(noDact, that.noDact) &&
                Objects.equals(idDisp, that.idDisp) &&
                Objects.equals(idEstaCred, that.idEstaCred) &&
                Objects.equals(idEsta, that.idEsta) &&
                Objects.equals(idTipo, that.idTipo) &&
                Objects.equals(usrCrea, that.usrCrea) &&
                Objects.equals(fchCrea, that.fchCrea) &&
                Objects.equals(usrModi, that.usrModi) &&
                Objects.equals(fchModi, that.fchModi) &&
                Objects.equals(usrOpeCrea, that.usrOpeCrea) &&
                Objects.equals(usrOpeModi, that.usrOpeModi);
    }

    @Override
    public int hashCode() {

        return Objects.hash(idCredTemp, usua, pass, nomOpe, apePate, apeMate, noDact, idDisp, idEstaCred, idEsta, idTipo, usrCrea, fchCrea, usrModi, fchModi, usrOpeCrea, usrOpeModi);
    }
}
